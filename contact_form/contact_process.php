<?php

//include dirname(dirname(__FILE__)).'/mail.php';
require(dirname(dirname(__FILE__))."/_lib/class.phpmailer.php");
$mail = new PHPMailer();

error_reporting (E_ALL ^ E_NOTICE);

$post = (!empty($_POST)) ? true : false;


if($post)
{
	include 'email_validation.php';
	$name = stripslashes($_POST['name']);
	$phone = stripslashes($_POST['phone']);
	$email = trim($_POST['email']);
	$subject = stripslashes($_POST['subject']);
	$message = stripslashes($_POST['message']);
	$error = '';
	$header = '';

	// Check name
	if(!$name){
		$error .= 'Por favor ingrese su Nombre.<br />';
	}

	if(!$phone){
		$error .= 'Por favor ingrese su Teléfono.<br />';
	}

	// Check email
	if(!$email){
		$error .= 'Por favor ingrese su E-Mail.<br />';
	}

	if($email && !ValidateEmail($email)){
		$error .= 'Por favor ingrese un E-Mail válido.<br />';
	}

	// Check message (length)
	if(!$message || strlen($message) < 10){
		$error .= "Por favor ingrese su mensaje. Debe tener al menos 10 caracteres.<br />";
	}

	if(!$error){
		$message .= "\r\n"."Teléfono: ".$phone. "\r\n"."Mail: ".$email. "\r\n"."Nombre: ".$name;
						
		//$mail =	mail(CONTACT_FORM, $subject, $message,$header);
		
		$mail->Host = "localhost";
		$mail->From = CONTACT_FORM;
		$mail->FromName = "Contacto Formulario Web";
		$mail->Subject = $subject;
		$mail->AddAddress("dennis.araya@gmail.com");
		$mail->Body = $message;
		
		//send the message, check for errors
		if (!$mail->send()) {
			echo "Error: " . $mail->ErrorInfo;
		}else{
			echo "OK";
		}
		exit;
	}
	else{
		echo '<div class="notification_error">'.$error.'</div>';
	}

}
?>
